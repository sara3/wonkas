$(document).ready(function() {

    $("#numero").prop("readonly", true);

    const numero = document.getElementById('numero');
    const max = $("#numero").attr('max');

    if (parseInt(max) <= 0) {
        $("#aggiungiCarrello").prop("disabled", true);
        $("#meno").prop("disabled", true);
        $("#piu").prop("disabled", true);
    }


    $("#meno").click(function(e) {
        e.preventDefault();
        if (numero.value > 1) {
            numero.value--;
        }
    });

    $("#piu").click(function(e) {
        e.preventDefault();

        if (parseInt(numero.value) < parseInt(max)) {
            numero.value++;
        }
    });

    document.getElementById("aggiungiCarrello").onclick = function() {
        document.getElementById("aggiungiCarrello").submit();
    }

});