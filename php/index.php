<?php

require_once("bootstrap.php");


$templateParams["titolo"] = "Home";
$templateParams["nome"] = "lista-articoli.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/prodotti.js");
$templateParams["prodCioc"] = $dbh->getRandomChoco(1);
$templateParams["prodCan"] = $dbh->getRandomCandy(1);
$templateParams["prodOth"] = $dbh->getRandomOther(1);


require("template/base.php");



?>