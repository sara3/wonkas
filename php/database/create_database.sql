-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Wonka
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Wonka
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wonka` DEFAULT CHARACTER SET utf8 ;
USE `wonka` ;

-- -----------------------------------------------------
-- Table `wonka`.`tipologia
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wonka`.`tipologia` (
  `nomeTipologia` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`nomeTipologia`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `wonka`.`prodotto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wonka`.`prodotto` (
  `codProdotto` INT NOT NULL AUTO_INCREMENT,
  `nomeProdotto` VARCHAR(100) NOT NULL,
  `descrizione` VARCHAR(512) NOT NULL,
  `quantità` INT NOT NULL DEFAULT 0,
  `prezzo`INT NOT NULL,
  `foto`  VARCHAR(500),
  `nomeTipologia` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`codProdotto`),
  CONSTRAINT `fk_tipologia`
    FOREIGN KEY (`nomeTipologia`)
    REFERENCES `wonka`.`tipologia` (`nomeTipologia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `wonka`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wonka`.`user` (
  `username` VARCHAR(20) NOT NULL,
  `nome` VARCHAR(20) NOT NULL,
  `cognome` VARCHAR(20) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `venditore` BOOLEAN DEFAULT 0 NOT NULL,
  `punti` INT DEFAULT 0 NOT NULL,
  `indirizzoPrincipale` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`username`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `wonka`.`prodotto_acquistato`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wonka`.`prodotto_acquistato` (
  `quantitàComprata` INT NOT NULL,
  `statoConsegna` VARCHAR(50) NOT NULL,
  `data_ora` DATETIME NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `codProdotto` INT NOT NULL,
  `indirizzo` VARCHAR(100) NOT NULL,
  CONSTRAINT `fk_username`
    FOREIGN KEY (`username`)
    REFERENCES `wonka`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_prodotto`
    FOREIGN KEY (`codProdotto`)
    REFERENCES `wonka`.`prodotto` (`codProdotto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    PRIMARY KEY (`data_ora`,`username`,`codProdotto`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `wonka`.`prodotto_carrello`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wonka`.`prodotto_carrello` (
  `data_ora` DATETIME NOT NULL,
  `quantitàInserita` INT NOT NULL,
  `userCarrello` VARCHAR(20) NOT NULL,
  `prodottoCarrello` INT NOT NULL,
  CONSTRAINT `fk_username_carrello`
    FOREIGN KEY (`userCarrello`)
    REFERENCES `wonka`.`user` (`username`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_prodotto_carrello`
    FOREIGN KEY (`prodottoCarrello`)
    REFERENCES `wonka`.`prodotto` (`codProdotto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    PRIMARY KEY (`data_ora`,`userCarrello`,`prodottoCarrello`))
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
