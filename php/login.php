<?php

require_once("bootstrap.php");

if(isset($_POST["username"]) && isset($_POST["password"])){
    $password = hash_hmac('sha256', $_POST["password"], $_POST["username"]);
    $login_result = $dbh->checkLogin($_POST["username"], $password);
    if(count($login_result)==0){
        $templateParams["erroreLogin"] = "Credenziali errate!";
    }else{
        registerLoggedUser($login_result[0]);
    }

}

if(isUserLoggedIn()){
    $loginVenditore_result = $dbh->checkLoginVenditore($_SESSION["username"]);
    if(count($loginVenditore_result)>0){
        $templateParams["titolo"] = "Area Venditore";
        $templateParams["nome"] = "admin.php";
    }else{
        $templateParams["titolo"] = "Profilo";
        $templateParams["nome"] = "profilo.php";
    }

}else{
    $templateParams["titolo"] = "Login";
    $templateParams["nome"] = "login-form.php";
}


require("template/base.php");



?>