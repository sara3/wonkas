<?php

require_once("bootstrap.php");

$idProdotto = -1;
if(isset($_GET["id"])){
    $idProdotto = $_GET["id"];
}

if(isUserLoggedIn() && isset($_GET["id"])){
    $loginVenditore_result = $dbh->checkLoginVenditore($_SESSION["username"]);
    if(count($loginVenditore_result)==1){
       $dbh->deleteProduct($_GET["id"]);
       header("Location: login.php");
    }

}else{
    header("Location: login.php");
}


require("template/base.php");

?>