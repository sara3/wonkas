<?php

require_once("bootstrap.php");

$templateParams["titolo"] = "Caramelle";
$templateParams["nome"] = "prodotti-tipologiaCar.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/prodotti.js");
$templateParams["prodottiCaramelle"] = $dbh->getCandyP();

require("template/base.php");

?>