<form action = "#" method = "POST">
    <h2>Login</h2>
    <?php if(isset($templateParams["erroreLogin"])): ?>
        <p><?php echo $templateParams["erroreLogin"];?></p>
     <?php endif;?>   
    <ul>
        <li>
            <label for:"uname">Username: </label>
            <input type="text" id= "username" name="username">
        </li>
        <li>
            <label for:"psw">Password: </label>
            <input type="password" id= "password" name="password">
        </li>
        <li>
            <a id = "registrazione" href="registrazione.php">Registrati</a>
            <input type="submit" class="button" name= "submitbutton" value="Invia" />
        </li>
    </ul>
</form>