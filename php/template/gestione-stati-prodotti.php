<h2>Gestisci spedizioni</h2>

<?php if(count($templateParams["prodottiAcquistati"])==0): ?>
    <p> <?php echo $_SESSION["nome"];?> non ci sono prodotti acquistati da spedire! </p>
<?php else: ?>

<?php foreach($templateParams["prodottiAcquistati"] as $prodotto): ?>
<article>
    <header>
    </header>
    <div>
        <small>Codice prodotto: </small>
        <strong><?php echo $prodotto["codProdotto"]; ?></strong>
    </div>
    <div>
        <small>Quantità: </small>
        <strong><?php echo $prodotto["quantitàComprata"]; ?></strong>
    </div>
    <div>
        <small>Data acquisto: </small>
        <strong><?php echo $prodotto["data_ora"]; ?></strong>
    </div>
    <div>
        <small>Indirizzo di consegna: </small>
        <strong><?php echo $prodotto["indirizzo"]; ?></strong>
    </div>
    <footer>
        <div>
            <a id = "cambiaStato" class= "button" href="cambia-stato.php?id=<?php echo $prodotto["username"]; ?>&cod=<?php echo $prodotto["codProdotto"]; ?>"> SPEDISCI</a>      
        </div>

    </footer>
</article>
<?php endforeach; endif;?>