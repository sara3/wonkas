<form action="insert-new-product.php" method="POST" enctype="multipart/form-data">
    <h2>Modifica prodotto</h2>   
    <ul>
        <li>
            <label for="codProdotto">Codice:</label><input readonly="text" id="codProdotto" name="codProdotto" value="<?php echo $templateParams["prodotto"][0]["codProdotto"]; ?>" />
        </li>
        <li>
            <label for="nomeProdotto">Nome:</label><input readonly="text" id="nomeProdotto" name="nomeProdotto" value="<?php echo $templateParams["prodotto"][0]["nomeProdotto"]; ?>" />
        </li>
        <li>
            <label for="quantità">Quantità:</label><textarea id="quantità" name="quantità"><?php echo $templateParams["prodotto"][0]["quantità"]; ?></textarea>
        </li>
        <li>
            <input type="submit" class = "button" name="submit" value="Salva" />
            <a href="login.php">Annulla</a>
        </li>
    </ul>
    <input type = "hidden" name=" action" value="2" />
    <input type="hidden" name="oldimg" value="<?php echo$templateParams["prodotto"][0]["foto"]; ?>" />
</form>