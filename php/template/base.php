<!DOCTYPE html>
<html lang="it">
<head>
    <title>Wonka's - <?php echo $templateParams["titolo"]; ?></title>
   <link rel="stylesheet" type="text/css" href="./css/style2.css">
   <link rel="icon" type="image/png" href="<?php echo UPLOAD_DIR;?>/logo2.png" >
   <?php
    if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
</head>
<body>
    <header>
        <h1>Wonka's</h1>
    </header>
    <nav>
        <ul>
            <li><a href="index.php">Store</a></li>
            <li><a href="cioccolato.php">Cioccolato</a></li>
            <li><a href="caramelle.php">Caramelle</a></li>
            <li><a href="altro.php">Altro</a></li>
            <li> <a href= "login.php"> <img class= "log" src='<?php echo UPLOAD_DIR; ?>icona_generica.svg' alt=""> </a></li>
            <li> <a href= "carrello.php"> <img class= "log" src='<?php echo UPLOAD_DIR; ?>carrello.png' alt=""> </a></li>
        </ul>
    </nav>
    <main>
    <?php
    if(isset($templateParams["nome"])){
        require($templateParams["nome"]);
    }
    ?>
    </main>
    <footer>
        <table>
            <tr>
                <th>Autori</th>
                <th>e-mail</th>
                <th>Matricola</th>
            </tr>
            <tr>
                <th id=sara> Bernabini Sara</th>
                <th id=ms> sara.bernabini3@studio.unibo.it </th>
                <th id=mats> 0000889156</th>
            </tr>
            <tr>
                <th id=michele> Monti Michele   </th>
                <th id=mm> michele.monti7@studio.unibo.it </th>
                <th id=matm> 0000892962</th>
            </tr>
        </table>
        
        <p>Link utili: <br/> Dr. Wilbur Wonka Sr. Dentist <br/> </p>
        <p>Il sito ha solo fini didattici <br/></p>
    </footer>
</body>
</html>