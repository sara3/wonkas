<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$templateParams["prodotto"]["foto"]; ?>" alt=""> 
        </div>
        <h2><?php echo $templateParams["prodotto"]["nomeProdotto"]; ?></h2>
        <span> Tipologia: <?php echo $templateParams["prodotto"]["nomeTipologia"]; ?></span>
        <br/>
        <span>Pezzi rimasti: <?php echo $templateParams["prodotto"]["quantità"]; ?></span>
        <h3>Costo: <?php echo $templateParams["prodotto"]["prezzo"]; ?> € </h3>
        <form >
            <div>
                <span>Quantità: </span>
                <button id = "meno" type="button" class="quantità"> - </button>
                <label for= "numero" id="lblNum"></label>
                <input type="number" id = "numero" name = "numero" value="1" max = "15"  />
                <button id = "piu" type="button" class="quantità"> + </button>
            </div>
            <?php if(!isset($_SESSION["username"])):?>
                <strong> Esegui il login per poter aggiungere prodotti al carrello!</strong>
            <?php else: ?>
            <div>
                <a id = "aggiungiCarrello" class= "insert" href="insert-prodotto-carrello.php?id=<?php echo $templateParams["prodotto"]["codProdotto"]; ?>&numero=3"> Aggiungi al Carrello</a>      
            </div>
            <?php endif; ?>
        </form>
        <span>
            <?php echo $templateParams["prodotto"]["descrizione"]; ?>
        </span>
    </header>
</article>
 

        