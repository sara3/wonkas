<?php foreach($templateParams["prodottiCaramelle"] as $caramelle): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$caramelle["foto"]; ?>" alt="" /> 
        </div>
        <h2><?php echo $caramelle["nomeProdotto"]; ?></h2>
    </header>
    <footer>
        <a href="more.php?id=<?php echo $caramelle["codProdotto"]; ?>">Please, tell me more!</a>
    </footer>
</article>
<?php endforeach; ?>
