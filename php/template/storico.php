<h2>Riepilogo Acquisti</h2>

<?php if(count($templateParams["storicoOrdini"])==0): ?>
    <p> <?php echo $_SESSION["nome"];?> non ci sono prodotti acquistati da visualizzare.  </p>
<?php else: ?>

<?php foreach($templateParams["storicoOrdini"] as $storico): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.($dbh->getNomeFotoProdotto($storico["codProdotto"])[0]["foto"]); ?>" alt=""> 
        </div>
        <h3><?php echo $dbh->getNomeFotoProdotto($storico["codProdotto"])[0]["nomeProdotto"]; ?></h3>
    </header>
    <div>
        <small>Quantità: </small>
        <strong><?php echo $storico["quantitàComprata"]; ?></strong>
    </div>
    <div>
        <small>Data acquisto: </small>
        <strong><?php echo $storico["data_ora"]; ?></strong>
    </div>
    <div>
        <small>Indirizzo di consegna: </small>
        <strong><?php echo $storico["indirizzo"]; ?></strong>
    </div>
    <div>
        <small>Stato di consegna: </small>
        <strong><?php echo $storico["statoConsegna"]; ?></strong>
    </div>
    <footer>
        <div>
            <a id = "seemore" class= "button" href="more.php?id=<?php echo $storico["codProdotto"]; ?>"> Compra di nuovo</a>      
        </div>

    </footer>
</article>
<?php endforeach; endif;?>
