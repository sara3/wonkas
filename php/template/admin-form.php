<a class = "inserisci" href="gestisci-prodotti.php">Nuovo prodotto</a>
<a class = "inserisci" href = "gestisci-spedizioni.php">Gestisci spedizioni </a>
<a id = "logout" class= "logout" href="logout.php">Logout</a>
<?php foreach($templateParams["tuttiProdotti"] as $all): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$all["foto"]; ?>" alt=""> 
            <h2><?php echo $all["nomeProdotto"]; ?></h2>
        </div>
    </header>
    <footer>
        <a class = "button" href="modifica.php?id=<?php echo $all["codProdotto"]; ?>">Modifica</a>
        <a class = "button" href="elimina.php?id=<?php echo $all["codProdotto"]; ?>">Elimina</a>
    </footer>
</article>
<?php endforeach; ?>