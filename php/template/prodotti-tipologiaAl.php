<?php foreach($templateParams["prodottiAltro"] as $altro): ?>
<article>
    <header>
        <div>
            <img src="<?php echo UPLOAD_DIR.$altro["foto"]; ?>" alt="" /> 
        </div>
        <h2><?php echo $altro["nomeProdotto"]; ?></h2>
    </header>
    <footer>
        <a href="more.php?id=<?php echo $altro["codProdotto"]; ?>">Please, tell me more!</a>
    </footer>
</article>
<?php endforeach; ?>
