<?php

require_once("bootstrap.php");

$templateParams["titolo"] = "Prodotto";
$templateParams["nome"] = "singolo-prodotto.php";
$templateParams["js"] = array("js/jquery-3.4.1.min.js", "js/bottoniMore.js");

$idProdotto = -1;
if(isset($_GET["id"])){
    $idProdotto = $_GET["id"];
}
$templateParams["prodotto"] = $dbh->getProductById($idProdotto)[0];
//gestione controllo id che non esiste

require("template/base.php");
?>