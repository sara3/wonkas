<?php

require_once("bootstrap.php");

if (isset($_POST["username"]) && isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["mail"]) && isset($_POST["password"]) && isset($_POST["address"])) {
    $password = hash_hmac('sha256', $_POST["password"], $_POST["username"]);
    if($dbh->registrazioneUtente($_POST["username"], $_POST["nome"], $_POST["cognome"], $_POST["mail"], $password, $_POST["address"])){
        $_SESSION["username"] = $_POST["username"];
        $_SESSION["nome"] = $_POST["nome"];
        $_SESSION["cognome"] = $_POST["cognome"];
        $_SESSION["punti"] = 0;
        $_SESSION["venditore"] = 0;
        header("Location: profilo.php");
    }
    else{
        header("location: login.php");
    }
}

require("template/base.php");
?>